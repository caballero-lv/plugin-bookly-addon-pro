<?php
namespace BooklyPro\Frontend\Modules\WooCommerce;

use Bookly\Lib as BooklyLib;
use Bookly\Frontend\Modules\Booking\Lib\Errors;

class Ajax extends Controller
{
    /**
     * @inheritDoc
     */
    protected static function permissions()
    {
        return array( '_default' => 'anonymous' );
    }

    /**
     * Add product to cart
     *
     * return string JSON
     */
    public static function addToWoocommerceCart()
    {
        $userData = new BooklyLib\UserBookingData( self::parameter( 'form_id' ) );

        if ( $userData->load() ) {
            $state = self::addToCart( $userData );
            $state === true
                ? wp_send_json_success( array( 'target_url' => wc_get_cart_url() ) )
                : wp_send_json_error( array( 'error' => $state ) );

        }

        Errors::sendSessionError();
    }

}